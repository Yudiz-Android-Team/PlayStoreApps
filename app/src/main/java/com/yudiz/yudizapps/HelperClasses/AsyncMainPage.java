package com.yudiz.yudizapps.HelperClasses;

//https://play.google.com/store/search?q=yudiz%20solutions&c=apps

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.yudizandroidapps.Model.DataModel;
import com.google.gson.Gson;
import com.yudiz.yudizapps.listeners.AsyncMainPageCallbackListeners;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class AsyncMainPage extends AsyncTask<String, String, String> {
    private Context mContext;
    private String urls1 = "https://play.google.com/store/search?q=yudiz%20solutions&c=apps";
    private String urls2 = "https://play.google.com/store/apps/developer?id=ITINDIA";
    private ProgressDialog progressDialog;
    AsyncMainPageCallbackListeners asyncMainPageCallbackListeners;

    public AsyncMainPage(Context mContext, AsyncMainPageCallbackListeners asyncMainPageCallbackListeners) {
        this.mContext = mContext;
        this.asyncMainPageCallbackListeners = asyncMainPageCallbackListeners;
    }

//    public void setUrl(String url) {
//        urls = url;
//    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
//        progressDialog = new ProgressDialog(mContext);
//        progressDialog.setMessage("Loading Data...");
//        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... args) {
        try {
            Document doc1 = Jsoup.connect(urls1).get();
            Elements list1 = doc1.select("div[class='card-content id-track-click id-track-impression']");
            DataModel dataModel = new DataModel();
            dataModel.data = new ArrayList<>();
            for (int i = 0; i < list1.size(); i++) {
                String developer = list1.get(i).select("a[class='subtitle']").text();
                if (!developer.equals("Yudiz Solutions Pvt Ltd"))
                    break;
                String storeurl = list1.get(i).attr("data-docid");
                String imageurl = "http://" + list1.get(i).select("img").attr("src").substring(2).replace("\\u003d", "=");
                String title = list1.get(i).child(2).child(1).attr("title");
                dataModel.data.add(new DataModel.Data(developer, storeurl, imageurl, title));
            }

            Document doc2 = Jsoup.connect(urls2).get();
            Elements list2 = doc2.select("div[class='card-content id-track-click id-track-impression']");
            for (int i = 0; i < list2.size(); i++) {
                String developer = list2.get(i).select("a[class='subtitle']").text();
                String packagename = list2.get(i).attr("data-docid");
                String image = "http://" + list2.get(i).select("img").attr("src").substring(2).replace("\\u003d", "=");
                String title = list2.get(i).child(2).child(1).attr("title");
                dataModel.data.add(new DataModel.Data(developer, packagename, image, title));
            }

            Log.e("Tag", "Apps list >> " + new Gson().toJson(dataModel));
            return new Gson().toJson(dataModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);
//        if (progressDialog != null)
//            progressDialog.dismiss();
        asyncMainPageCallbackListeners.callback(aVoid);

    }


}
