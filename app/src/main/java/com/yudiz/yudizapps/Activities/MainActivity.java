package com.yudiz.yudizapps.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.yudiz.yudizapps.HelperClasses.AsyncMainPage;
import com.yudiz.yudizapps.R;
import com.yudiz.yudizapps.listeners.AsyncMainPageCallbackListeners;


public class MainActivity extends AppCompatActivity implements AsyncMainPageCallbackListeners {
    AsyncMainPage asyncMainPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        asyncMainPage = (AsyncMainPage) new AsyncMainPage(this, MainActivity.this).execute();

    }


    @Override
    public void callback(String result) {

    }
}
