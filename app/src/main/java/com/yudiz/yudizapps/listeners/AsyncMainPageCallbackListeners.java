package com.yudiz.yudizapps.listeners;

/**
 * Created by yudizsolutions on 11/06/18.
 */

public interface AsyncMainPageCallbackListeners {

    void callback(String result);

}
