package com.example.yudizandroidapps.HelperClasses;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.example.yudizandroidapps.Adapters.AppsAdapter;
import com.example.yudizandroidapps.Model.DataModel;
import com.example.yudizandroidapps.R;
import com.google.gson.Gson;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class YudizApps extends FrameLayout implements View.OnClickListener {

    private RecyclerView rvList;
    private TextView tvWait;
    private int yudizTheme = 0;
    private Context context;

    public YudizApps(Context context) {
        super(context);
        init(context, null);
    }

    public YudizApps(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);

    }

    public YudizApps(@NonNull Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, @Nullable AttributeSet attrs) {

        TypedArray yudizAttrib = context.getTheme().obtainStyledAttributes(attrs, R.styleable.YudizApps, 0, 0);
        yudizTheme = yudizAttrib.getInteger(R.styleable.YudizApps_themetype, 0);
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.yudiz_app_layout, this, true);


        initViews(view);
        initTask(context);
    }

    private void initViews(View view) {
        rvList = view.findViewById(R.id.rv_app_list);
        tvWait = view.findViewById(R.id.tv_wait);

    }

    private void initTask(Context context) {
        this.context = context;
        if (isNetworkAvailable(context)) {
            loadList();
        } else {
            tvWait.setVisibility(VISIBLE);
            tvWait.setText(R.string.connection_required);
            tvWait.setOnClickListener(this);
        }
        if (yudizTheme == 2) {
            rvList.setLayoutManager(new GridLayoutManager(getContext(), 3));

        } else if (yudizTheme == 1) {
            rvList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        } else {
            rvList.setLayoutManager(new LinearLayoutManager(getContext()));
        }

    }

    private void loadList() {
        tvWait.setText(R.string.please_wait);
        new loadApps().execute();
        tvWait.setOnClickListener(null);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tv_wait) {
            initTask(context);

        }
    }

    @SuppressLint("StaticFieldLeak")
    private class loadApps extends AsyncTask<Void, Void, Void> {

        private URL url;
        private HttpURLConnection urlConnection = null;
        private String json = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            tvWait.setVisibility(VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                url = new URL("https://gitlab.com/Yudiz-Android-Team/PlayStoreApps/raw/master/yudizandroidapps/src/main/assets/apps.json");
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader isw = new InputStreamReader(in);

                int data = isw.read();
                while (data != -1) {
                    char current = (char) data;
                    data = isw.read();
                    json += current;
//                    System.out.print(current);
                }
            } catch (Exception e) {
                e.printStackTrace();
//                tvWait.setText(e.getMessage());
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Gson gson = new Gson();
            DataModel dataModel = gson.fromJson(json, DataModel.class);
            if (dataModel != null && dataModel.data != null && dataModel.data.size() > 0) {
                rvList.setAdapter(new AppsAdapter(getContext(), dataModel, yudizTheme));
                tvWait.setVisibility(GONE);
            } else {
                tvWait.setText(R.string.something_wrong);
            }

        }
    }

    private boolean isNetworkAvailable(Context context) {
        if (context == null)
            return false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;
        if (connectivityManager != null) {
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}