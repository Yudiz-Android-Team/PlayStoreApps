package com.example.yudizandroidapps.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DataModel {
    @SerializedName("data")
    @Expose
    public List<Data> data = null;

    public static class Data {
        @SerializedName("name")
        @Expose
        public String name;
        @SerializedName("store_url")
        @Expose
        public String storeUrl;
        @SerializedName("image_url")
        @Expose
        public String imageUrl;
        @SerializedName("developer")
        @Expose
        public String developer;

        public Data(String developer, String storeUrl, String imageUrl, String name) {
            this.name = name;
            this.storeUrl = storeUrl;
            this.imageUrl = imageUrl;
            this.developer = developer;
        }
    }
}
