package com.example.yudizandroidapps.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.yudizandroidapps.Model.DataModel;
import com.example.yudizandroidapps.R;

import java.nio.charset.Charset;


public class AppsAdapter extends RecyclerView.Adapter<AppsAdapter.ViewHolder> {

    private LayoutInflater inflater;
    private DataModel dataModel;
    private Context context;
    private int yudizTheme;

    public AppsAdapter(Context context, DataModel dataModel, int yudizTheme) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.dataModel = dataModel;
        this.yudizTheme = yudizTheme;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (yudizTheme == 2) {
            return new ViewHolder(inflater.inflate(R.layout.row_apps_grid, parent, false));
        } else if (yudizTheme == 1) {
            return new ViewHolder(inflater.inflate(R.layout.row_apps_grid, parent, false));
        } else {
            return new ViewHolder(inflater.inflate(R.layout.row_apps_linear, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.set(position);
    }

    @Override
    public int getItemCount() {
        return dataModel.data.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvAppName;
        private TextView tvDevelopedBy;
        private Button btnInstall;
        private ImageView imgVwAppLogo;

        ViewHolder(View view) {
            super(view);
            tvAppName = view.findViewById(R.id.tv_app_name);
            tvDevelopedBy = view.findViewById(R.id.tv_developed_by);
            btnInstall = view.findViewById(R.id.install_app_btn);
            imgVwAppLogo = view.findViewById(R.id.iv_app_logo);
        }

        public void set(final int position) {
            tvAppName.setText(dataModel.data.get(position).name);
            tvDevelopedBy.setText(dataModel.data.get(position).developer);
            String imageurl = new String(dataModel.data.get(position).imageUrl.getBytes(Charset.forName("UTF-8")), Charset.forName("UTF-8"));
//            Log.d("Replaced UTF", imageurl);
//            Log.d("Replaced String", dataModel.data.get(position).imageUrl.replace("\\u003d", "="));
            Glide.with(context).load(imageurl)
                    .apply(new RequestOptions().placeholder(R.drawable.placeholder))
                    .thumbnail(0.5f)
                    .into(imgVwAppLogo);

            btnInstall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + dataModel.data.get(position).storeUrl)));
                }
            });

        }
    }

}